// Create a Chess baord of a specified size
// node create --size 8

const { createCanvas} = require("canvas");
const { program } = require('commander');
const params = require('./params') ;


const wsq = params.Sq_Size ;
const hsq = params.Sq_Size ;

program
    .description("Create a chess board of a specified size")
    .option('-s, --size <sizeval>','size of the chess board' , 8 )
    ;

program.parse(process.argv);
const options = program.opts() ;

let boardsize = options.size ;
let width = boardsize * wsq ;
const height = boardsize * hsq ;

console.log('Board size ',boardsize,' Height ' , height , ' Width ' , width );
const canvas = createCanvas(width, height);
const context = canvas.getContext("2d");
var white = new Boolean(true) ;

let rtl = 0 ;
let ctl = 0 ;

for (let r=0; r<boardsize; r++) {
    ctl=0;
    for (let c=0; c<boardsize; c++) {   
        if (white) {
           context.fillStyle = "yellow" ;
           white=false;
        } else {
            context.fillStyle = "red" ;
            white=true;
        }
        context.fillRect(ctl,rtl,wsq,hsq)
        ctl = wsq + ctl ;
    }
    if (white) {
        white=false;
     } else {
        white=true;
     }
     rtl = rtl + hsq ;
}

const buffer = canvas.toBuffer("image/png");

const fs = require("fs");
fs.writeFileSync("./board.png", buffer);
