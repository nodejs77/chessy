// Loads the basic image of a chessboad and
// places a piece in a specified number of 
// squares.
//
// usage:
// node board/addb.js --board 8 <row,col> <row,col> ...
//
// Base board  : board.png
// With pieces : board-new.png

const { program } = require ( 'commander' );
var Jimp = require ('jimp') ;
const params = require('./params') ;


program
    .description("Add bishops at a specified number of squares")
    .option('-c, --count <npieces>','number of pieces' , 4)
    .option('-b, --board <size>' , 'board size' , 8 )
    .option('-p, --piece <name>' , 'name of the piece to place' , 'bishop')
    .argument('<squares...>')
    ;
program.parse() ;
options = program.opts() ;
let numpieces = options.count ;
let boardsize = options.board ;

const wsq = params.Sq_Size ;
const hsq = params.Sq_Size ;

let pieceimagefile = 'bishop.png' ;
if (options.piece == 'knight') {
    pieceimagefile = 'knight.jpeg' ;
}

Jimp.read("board.png" , (err,board) => {
    if (err) throw err ;
    Jimp.read(pieceimagefile, (err, pieceimage) => {
    if (err) throw err;
    pieceimage.resize(wsq,hsq);
    program.args.forEach((sq) => {
        rnum = sq.split(',')[0] ;
        cnum = sq.split(',')[1] ;
        console.log('Row ',rnum,' Col ', cnum);
        board.blit(pieceimage,(cnum)*wsq,(rnum)*hsq);
        });
    board.write('board-new.png'); // save
    });
});
